FROM golang:1.11.0 as builder

WORKDIR /go/src/gitlab.com/schnjess/user-cli

COPY . .

RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .

FROM debian:latest

RUN mkdir /app
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/schnjess/user-cli/user-cli .

ENTRYPOINT ["./user-cli"]

CMD ["./user-cli"]
