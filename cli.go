package main

import (
	"log"
	"os"

	pb "gitlab.com/schnjess/user-service/proto/auth"
	microclient "github.com/micro/go-micro/client"
	"golang.org/x/net/context"
	micro "github.com/micro/go-micro"
)


func main() {



	srv := micro.NewService(

		micro.Name("go.micro.srv.user-cli"),
		micro.Version("latest"),
	)

	//init will pass the command line flags
	srv.Init()

	// Create new greeter client
	client := pb.NewAuthService("go.micro.srv.user", microclient.DefaultClient)

  name := "Jesse Schneider"
	email := "jessewschneider96@gmail.com"
	password := "password123abc"
	company := "Project-T"

	r, err := client.Create(context.TODO(), &pb.User {
		Name: name,
		Email: email,
		Password: password,
		Company: company,
	})
	if err != nil {
		log.Fatalf("Could not create: %v", err)
	}
	log.Printf("Created: %s", r.User.Id)

	getAll, err := client.GetAll(context.Background(), &pb.Request{})
	if err != nil {
		log.Fatalf("Could not list users: %v", err)
	}
	for _, v := range getAll.Users {
		log.Println(v)
	}

	authResponse, err := client.Auth(context.TODO(), &pb.User {
		Email: email,
		Password: password,
	})

	if err != nil {
		log.Fatalf("Could not authenticate user: %s error: %v\n", email, err)
	}

	log.Printf("Your access token is: %s \n", authResponse.Token)

	//let's just exit because
	os.Exit(0)
}
